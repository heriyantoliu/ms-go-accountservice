build:
	CGO_ENABLED=0 go build -o accountservice-linux-amd64
	sudo docker build -t accountservice .
run:
	sudo docker run -p 6767:6767 --rm accountservice
registerSwarm:
	sudo docker service rm accountservice
	sudo docker service create \
	--name=accountservice \
	--replicas=1 \
	--network=my_network \
	-p=6767:6767 \
	accountservice