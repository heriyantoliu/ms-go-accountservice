module accountService

go 1.12

require (
	github.com/boltdb/bolt v1.3.1
	github.com/gorilla/mux v1.7.3
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	github.com/stretchr/testify v1.3.0
	golang.org/x/sys v0.0.0-20190804053845-51ab0e2deafa // indirect
)
